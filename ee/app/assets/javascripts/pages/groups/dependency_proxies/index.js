import DependencyProxy from 'ee/dependency_proxy';

document.addEventListener('DOMContentLoaded', () => new DependencyProxy());
